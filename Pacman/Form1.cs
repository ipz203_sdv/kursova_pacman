﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Media;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassLibrary1;

namespace Pacman
{
    public partial class Form1 : Form
    {
        private Image pacmanImg;
        private Player player;

        private Ghosts redGhost;
        private Ghosts pinkGhost;
        private Ghosts orangeGhost;
        private Ghosts blueGhost;
        Ghosts[] allGhosts = new Ghosts[4];

        Map m = new Map();

        private Keys lastkey;

        private bool reverseMove;

        public Form1()
        {
            InitializeComponent();
            timer1.Start();
            Init();
        }


        public void Init()
        {
            SoundPlayer startSound = new SoundPlayer(Properties.Resources.pacman_start);
            startSound.Play();
            pacmanImg = new Bitmap(Properties.Resources.Anim_sprites);
            player = new Player(pacmanImg, 300, 395);
            redGhost = new Ghosts(Properties.Resources.RedGhostAnim, 15, 15);
            pinkGhost = new Ghosts(Properties.Resources.PinkGhostAnim, -100, -100);
            orangeGhost = new Ghosts(Properties.Resources.OrangeGhostAnim, -100, -100);
            blueGhost = new Ghosts(Properties.Resources.BlueGhostAnim, -100, -100);
            allGhosts[0] = redGhost;
            allGhosts[1] = pinkGhost;
            allGhosts[2] = orangeGhost;
            allGhosts[3] = blueGhost;
            RedGhostMove.Start();
            PinkGhostMove.Start();
            OrangeGhostMove.Start();
            BlueGhostMove.Start();
            timer1.Start();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            m.DrawMap(g, player);
            player.PlayAnimation(g);
            redGhost.PlayAnimation(g);
            if (secondsFoEmergence >= 15)
                pinkGhost.PlayAnimation(g);

            if (secondsFoEmergence >= 30)
                orangeGhost.PlayAnimation(g);

            if (secondsFoEmergence >= 45)
                blueGhost.PlayAnimation(g);

        }

        SoundPlayer deathSound = new SoundPlayer(Properties.Resources.pacman_death);

        private void UpMove_Tick(object sender, EventArgs e)
        {
            DownMove.Stop();
            LeftMove.Stop();
            RightMove.Stop();

            if (PhysicsController.IsDeath(player, new Point(player.dirX, player.dirY), allGhosts))
            {
                UpMove.Stop();
                Lose();
                deathSound.Play();
            }

            if (!PhysicsController.IsCollide(player, new Point(player.dirX, player.dirY)))
            {
                player.posY += player.dirY;
            }
            else
            {
                player.posY += 4;
                UpMove.Stop();
            }

        }

        public void GameStop()
        {
            RedGhostMove.Stop();
            PinkGhostMove.Stop();
            OrangeGhostMove.Stop();
            BlueGhostMove.Stop();
            MoveDelay.Dispose();
            timer1.Dispose();
            pictureBox1.Visible = true;
            button1.Visible = true;
        }


        public void Lose()
        {
            GameStop();
            pictureBox1.Image = Properties.Resources.LoseImg;
        }

        private void DownMove_Tick(object sender, EventArgs e)
        {
            UpMove.Stop();
            LeftMove.Stop();
            RightMove.Stop();

            if (PhysicsController.IsDeath(player, new Point(player.dirX, player.dirY), allGhosts))
            {
                DownMove.Stop();
                Lose();
                deathSound.Play();
            }

            if (!PhysicsController.IsCollide(player, new Point(player.dirX, player.dirY)))
            {
                player.posY += player.dirY;
            }
            else
            {
                player.posY -= 4;
                DownMove.Stop();
            }
        }

        private void LeftMove_Tick(object sender, EventArgs e)
        {
            UpMove.Stop();
            RightMove.Stop();
            DownMove.Stop();

            if (PhysicsController.IsDeath(player, new Point(player.dirX, player.dirY), allGhosts))
            {
                LeftMove.Stop();
                Lose();
                deathSound.Play();
            }

            if (!PhysicsController.IsCollide(player, new Point(player.dirX, player.dirY)))
            {
                player.posX += player.dirX;
            }
            else
            {
                player.posX -= 4;
                LeftMove.Stop();
            }
        }

        private void RightMove_Tick(object sender, EventArgs e)
        {
            UpMove.Stop();
            LeftMove.Stop();
            DownMove.Stop();

            if (PhysicsController.IsDeath(player, new Point(player.dirX, player.dirY), allGhosts))
            {
                RightMove.Stop();
                Lose();
                deathSound.Play();
            }

            if (!PhysicsController.IsCollide(player, new Point(player.dirX, player.dirY)))
            {
                player.posX += player.dirX;
            }
            else
            {
                player.posX += 4;
                RightMove.Stop();
            }
        }

        private void MoveDelay_Tick(object sender, EventArgs e)
        {
            if (PhysicsController.IsTurn(player, new Point(player.dirX, player.dirY)))
            {
                switch (lastkey)
                {
                    case Keys.W:
                        {
                            LeftMove.Stop();
                            RightMove.Stop();
                            DownMove.Stop();
                            player.dirY = -3;
                            player.dirX = 0;
                            player.currentAnimation = 2;
                            UpMove.Start();
                            MoveDelay.Stop();
                            break;
                        }

                    case Keys.S:
                        {
                            UpMove.Stop();
                            LeftMove.Stop();
                            RightMove.Stop();
                            player.dirY = 3;
                            player.dirX = 0;
                            player.currentAnimation = 3;
                            DownMove.Start();
                            MoveDelay.Stop();
                            break;
                        }
                    case Keys.A:
                        {
                            UpMove.Stop();
                            LeftMove.Stop();
                            DownMove.Stop();
                            player.currentAnimation = 1;
                            player.dirX = -3;
                            player.dirY = 0;
                            RightMove.Start();
                            MoveDelay.Stop();
                            break;
                        }

                    case Keys.D:
                        {
                            UpMove.Stop();
                            RightMove.Stop();
                            DownMove.Stop();
                            player.currentAnimation = 0;
                            player.dirX = 3;
                            player.dirY = 0;
                            LeftMove.Start();
                            MoveDelay.Stop();
                            break;
                        }
                }
            }
        }

        int redGhostDir = 0;
        private void timer3_Tick(object sender, EventArgs e)
        {

            GhostsMove(redGhost, ref redGhostDir);

        }

       int pinkGhostDir = 2;
        private void timer4_Tick(object sender, EventArgs e)
        {
            GhostsMove(pinkGhost, ref pinkGhostDir);
        }


        int orangeGhostDir = 3;
        private void timer5_Tick(object sender, EventArgs e)
        {
            GhostsMove(orangeGhost, ref orangeGhostDir);
        }

        int blueGhostDir = 2;
        private void timer6_Tick(object sender, EventArgs e)
        {
            GhostsMove(blueGhost, ref blueGhostDir);
        }


        public void GhostsMove(Ghosts ghost, ref int a)
        {

            switch (a)
            {

                case 0:
                    {
                        if (!PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                        {
                            if (reverseMove)
                            {
                                ghost.currentAnimation = 4;
                                ghost.dirX = -3;
                            }
                            else
                            {
                                ghost.currentAnimation = 0;
                                ghost.dirX = 3;
                            }

                            ghost.MoveX();

                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posY >= ghost.posY)
                                    a = 2;
                                else
                                    a = 3;
                            }
                        }
                        else
                        {
                            if (reverseMove)
                                ghost.posX += 4;
                            else
                                ghost.posX -= 4;
                                if (player.posY >= ghost.posY)
                                    a = 2;
                                else
                                    a = 3;
                        }
                        break;
                    }
                case 1:
                    {
                        

                        if (!PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                        {
                            if (reverseMove)
                            {
                                ghost.currentAnimation = 4;
                                ghost.dirX = 3;
                            }
                            else
                            {
                                ghost.currentAnimation = 2;
                                ghost.dirX = -3;
                            }

                            ghost.MoveX();
                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posY >= ghost.posY)
                                    a = 2;
                                else
                                    a = 3;
                            }
                        }
                        else
                        {
                            if (reverseMove)
                                ghost.posX -= 4;
                            else
                                ghost.posX += 4;
                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posY > ghost.posY)
                                    a = 2;
                                else
                                    a = 3;
                            }
                        }
                        break;
                    }
                case 2:
                    {
                        if (!PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                        {
                            if (reverseMove)
                            {
                                ghost.currentAnimation = 4;
                                ghost.dirY = -3;
                            }
                            else
                            {
                                ghost.currentAnimation = 1;
                                ghost.dirY = 3;
                            }

                            ghost.MoveY();

                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posX < ghost.posX)
                                    a = 1;
                                else
                                    a = 0;
                            }
                        }
                        else
                        {
                            if (reverseMove)
                                ghost.posY += 4;
                            else
                                ghost.posY -= 4;

                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posX < ghost.posX)
                                    a = 1;
                                else
                                    a = 0;
                            }
                        }
                        break;
                    }
                case 3:
                    {

                        if (!PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                        {
                            if (reverseMove)
                            {
                                ghost.currentAnimation = 1;
                                ghost.dirY = 4;
                            }
                            else
                            {
                                ghost.currentAnimation = 3;
                                ghost.dirY = -3;
                            }

                            ghost.MoveY();

                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posX < ghost.posX)
                                    a = 1;
                                else
                                    a = 0;
                            }
                            break;
                        }
                        else
                        {
                            if (reverseMove)
                                ghost.posY -= 4;
                            else
                                ghost.posY += 4;

                            if (PhysicsController.IsTurn(ghost, new Point(ghost.dirX, ghost.dirY)) && !PhysicsController.IsCollide(ghost, new Point(ghost.dirX, ghost.dirY)))
                            {
                                if (player.posX < ghost.posX)
                                    a = 1;
                                else
                                    a = 0;
                            }
                        }
                        break;
                    }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            timer1.Interval = 70;

            switch (e.KeyCode)
            {
                case Keys.W:
                    {
                        lastkey = Keys.W;
                        MoveDelay.Start();
                        break;
                    }
                case Keys.S:
                    {
                        lastkey = Keys.S;
                        MoveDelay.Start();
                        break;
                    }
                case Keys.A:
                    {
                        lastkey = Keys.A;
                        MoveDelay.Start();
                        break;
                    }
                case Keys.D:
                    {
                        lastkey = Keys.D;
                        MoveDelay.Start();
                        break;
                    }
            }


            if (UpMove.Enabled || DownMove.Enabled)
            {
                switch (e.KeyCode)
                {
                    case Keys.W:
                        {
                            UpMove.Start();
                            player.currentAnimation = 2;
                            player.dirY = -3;
                            break;
                        }

                    case Keys.S:
                        {
                            DownMove.Start();
                            player.currentAnimation = 3;
                            player.dirY = 3;
                            break;
                        }
                }
            }
            else
            {
                switch (e.KeyCode)
                {
                    case Keys.A:
                        {
                            RightMove.Start();
                            player.currentAnimation = 1;
                            player.dirX = -3;
                            player.dirY = 0;
                            break;
                        }

                    case Keys.D:
                        {
                            LeftMove.Start();
                            player.currentAnimation = 0;
                            player.dirX = 3;
                            player.dirY = 0;
                            break;
                        }
                }
            }
        }


        private void timer1_Tick(object sender, EventArgs e)
        {
            InstantGhostsNow();
            SlowGhostsNow();
            Invalidate();
            if (m.Win())
            {
                GameStop();
                pictureBox1.Image = Properties.Resources.WinImg;
            }

            if (PhysicsController.IsDeath(player, new Point(player.dirX, player.dirY), allGhosts))
            {
                Lose();
                deathSound.Play();
            }

            if (secondsFoEmergence == 25)
            {
                pinkGhost.posX = 235;
                pinkGhost.posY = 215;
            }

            if (secondsFoEmergence == 40)
            {
                orangeGhost.posX = 235;
                orangeGhost.posY = 215;
            }

            if (secondsFoEmergence == 55)
            {
                blueGhost.posX = 235;
                blueGhost.posY = 215;
            }
        }

        int secondsFoEmergence = 0, secondsForSlow = 0;
        private void Secondomer_Tick(object sender, EventArgs e)
        {
            secondsFoEmergence++;
            secondsForSlow++;
        }


        public void SlowGhostsNow()
        {
            if (m.slowGhosts)
            {
                RedGhostMove.Interval = 45;
                PinkGhostMove.Interval = 45;
                OrangeGhostMove.Interval = 45;
                BlueGhostMove.Interval = 45;
                secondsForSlow = 0;
                reverseMove = true;
                m.slowGhosts = false;
            }
        }

        public void InstantGhostsNow()
        {
            if(m.slowGhosts == false)
            {
                if(secondsForSlow == 7)
                {
                    reverseMove = false;
                    RedGhostMove.Interval = 25;
                    PinkGhostMove.Interval = 25;
                    OrangeGhostMove.Interval = 25;
                    BlueGhostMove.Interval = 25;
                }
            }
        }
    }
}