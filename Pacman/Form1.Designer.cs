﻿
namespace Pacman
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.UpMove = new System.Windows.Forms.Timer(this.components);
            this.DownMove = new System.Windows.Forms.Timer(this.components);
            this.LeftMove = new System.Windows.Forms.Timer(this.components);
            this.RightMove = new System.Windows.Forms.Timer(this.components);
            this.MoveDelay = new System.Windows.Forms.Timer(this.components);
            this.RedGhostMove = new System.Windows.Forms.Timer(this.components);
            this.PinkGhostMove = new System.Windows.Forms.Timer(this.components);
            this.OrangeGhostMove = new System.Windows.Forms.Timer(this.components);
            this.BlueGhostMove = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.Secondomer = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // UpMove
            // 
            this.UpMove.Interval = 17;
            this.UpMove.Tick += new System.EventHandler(this.UpMove_Tick);
            // 
            // DownMove
            // 
            this.DownMove.Interval = 17;
            this.DownMove.Tick += new System.EventHandler(this.DownMove_Tick);
            // 
            // LeftMove
            // 
            this.LeftMove.Interval = 17;
            this.LeftMove.Tick += new System.EventHandler(this.LeftMove_Tick);
            // 
            // RightMove
            // 
            this.RightMove.Interval = 17;
            this.RightMove.Tick += new System.EventHandler(this.RightMove_Tick);
            // 
            // MoveDelay
            // 
            this.MoveDelay.Interval = 1;
            this.MoveDelay.Tick += new System.EventHandler(this.MoveDelay_Tick);
            // 
            // RedGhostMove
            // 
            this.RedGhostMove.Interval = 25;
            this.RedGhostMove.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // PinkGhostMove
            // 
            this.PinkGhostMove.Interval = 25;
            this.PinkGhostMove.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // OrangeGhostMove
            // 
            this.OrangeGhostMove.Interval = 25;
            this.OrangeGhostMove.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // BlueGhostMove
            // 
            this.BlueGhostMove.Interval = 25;
            this.BlueGhostMove.Tick += new System.EventHandler(this.timer6_Tick);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Orange;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("MV Boli", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(195, 382);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(137, 73);
            this.button1.TabIndex = 2;
            this.button1.Text = "Exit";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.Location = new System.Drawing.Point(93, 48);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(360, 328);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // Secondomer
            // 
            this.Secondomer.Enabled = true;
            this.Secondomer.Interval = 1000;
            this.Secondomer.Tick += new System.EventHandler(this.Secondomer_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(559, 621);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.pictureBox1);
            this.DoubleBuffered = true;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(575, 660);
            this.MinimumSize = new System.Drawing.Size(575, 660);
            this.Name = "Form1";
            this.Text = "PacmanGame";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form1_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Timer MoveDelay;
        public System.Windows.Forms.Timer UpMove;
        public System.Windows.Forms.Timer DownMove;
        public System.Windows.Forms.Timer LeftMove;
        public System.Windows.Forms.Timer RightMove;
        private System.Windows.Forms.Timer RedGhostMove;
        private System.Windows.Forms.Timer PinkGhostMove;
        private System.Windows.Forms.Timer OrangeGhostMove;
        private System.Windows.Forms.Timer BlueGhostMove;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Timer Secondomer;
    }
}

