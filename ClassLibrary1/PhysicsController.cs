﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class PhysicsController
    {

        public static bool IsCollide(Player player, Point dir)
        {
            for (int i = 0; i < Map.mapObjects.Count; i++)
            {
                var currObject = Map.mapObjects[i];
                Point delta = new Point();
                delta.X = (player.posX + player.playerSize / 2) - (currObject.position.X + currObject.size.Width / 2);
                delta.Y = (player.posY + player.playerSize / 2) - (currObject.position.Y + currObject.size.Height / 2);

                if (Math.Abs(delta.X) <= player.playerSize / 2 + currObject.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= player.playerSize / 2 + currObject.size.Height / 2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public static bool IsTurn(Player player, Point dir)
        {
            for (int i = 0; i < Map.turn.Count; i++)
            {
                var currObject = Map.turn[i];

                Point delta = new Point();
                delta.X = (player.posX + player.playerSize / 2) - (currObject.position.X + currObject.size.Width / 2);
                delta.Y = (player.posY + player.playerSize / 2) - (currObject.position.Y + currObject.size.Height / 2);


                if (Math.Abs(delta.X) <= player.playerSize / 2 + currObject.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= player.playerSize / 2 + currObject.size.Height / 2)
                    {
                        if (delta.X >= 0 && delta.X <= 3 && dir.X == 3)
                        {
                            return true;
                        }

                        if (delta.X <= 1 && delta.X >= -5 && dir.X == -3)
                        {
                            return true;
                        }

                        if (delta.Y >= 0 && delta.Y <= 3 && dir.Y == 3)
                        {
                            return true;
                        }

                        if (delta.Y < 2 && delta.Y >= -5 && dir.Y == -3)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }

        public static bool EatPoint(Player player, Point dir, MapEntity currObject, ref byte a)
        {
                Point delta = new Point();
                delta.X = (player.posX + player.playerSize / 2) - (currObject.position.X + currObject.size.Width / 2);
                delta.Y = (player.posY + player.playerSize / 2) - (currObject.position.Y + currObject.size.Height / 2);

            if (a == 2 || a == 3 || a == 8)
            {
                if (Math.Abs(delta.X) <= player.playerSize / 2 + currObject.size.Width / 2)
                {
                    if (Math.Abs(delta.Y) <= player.playerSize / 2 + currObject.size.Height / 2)
                    {
                        if (a == 2)
                        {
                            a = 1;
                        }
                        else
                        {
                            if(a == 8)
                            {
                                a = 1;
                                return true;
                            }
                            else
                            {
                                a = 4;
                            }
                        }
                    }
                }
            }
            return false;
        }

        public static bool IsDeath(Player player, Point dir, Ghosts[] ghost)
        {
            for(int i = 0; i < ghost.Length; i++)
            {
                Point delta = new Point();
                delta.X = (player.posX + player.playerSize / 2) - (ghost[i].posX + ghost[i].playerSize / 2);
                delta.Y = (player.posY + player.playerSize / 2) - (ghost[i].posY + ghost[i].playerSize / 2);

                if (Math.Abs(delta.X) <= player.playerSize / 2 + ghost[i].playerSize / 2)
                {
                    if (Math.Abs(delta.Y) <= player.playerSize / 2 + ghost[i].playerSize / 2)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
