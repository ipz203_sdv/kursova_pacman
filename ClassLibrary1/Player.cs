﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Player
    {
        protected Image sprites;

        public int posX { get; set; }
        public int posY { get; set; }
        protected int frameSize;
        public int runFrames { get; set; }

        protected int currentFrame;
        public int currentAnimation { get; set; }
        protected int currentLimit;

        protected int _playerSize = 30;

        public int playerSize
        {
            get
            {
                return _playerSize;
            }
        }

        public int dirX { get; set; }
        public int dirY { get; set; }

        public Player(Image sprites, int x, int y)
        {
            this.sprites = sprites;
            this.posX = x;
            this.posY = y;
            this.runFrames = 4;
            frameSize = 36;
            currentFrame = 0;
            currentAnimation = 0;
        }

        public virtual void PlayAnimation(Graphics g)
        {
            g.DrawImage(sprites, new Rectangle(new Point(posX, posY), new Size(playerSize, playerSize)), frameSize * currentFrame, frameSize * currentAnimation, frameSize, frameSize, GraphicsUnit.Pixel);

            if (0 <= currentAnimation && currentAnimation <= 3)
                currentLimit = runFrames;

            if (currentFrame < currentLimit - 1)
                currentFrame++;
            else currentFrame = 0;
        }
    }
}
