﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class MapEntity
    {
        public Point position { get; set; }
        public Size size { get; set; }

        public MapEntity(Point position, Size size)
        {
            this.position = position;
            this.size = size;
        }
    }
}
