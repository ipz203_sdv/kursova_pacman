﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary1
{
    public class Ghosts : Player
    {
        protected bool reverseMove { get; set; }

        public Ghosts(Image sprites, int x, int y) : base(sprites, x, y)
        {
            runFrames = 2;
            reverseMove = false;
        }

        public override void PlayAnimation(Graphics g)
        {
            base.PlayAnimation(g);
        }

        public void MoveX()
        {
            dirY = 0;
            posX += dirX;
        }

        public void MoveY()
        {
            dirX = 0;
            posY += dirY;
        }
    }

    
}
